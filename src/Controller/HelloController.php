<?php

namespace App\Controller;

use App\Service\LuckyNumber;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class HelloController
 * @package App\Controller
 * @Route("/page")
 */
class HelloController extends AbstractController
{
    /**
     * @Route(path="/hi/{name}", name="hello")
     * @param string $name
     * @param Request $request
     * @param LuckyNumber $luckyNumber
     * @return Response
     */
    public function hello(string $name, LuckyNumber $luckyNumber, Request $request): Response
    {
        $number = $luckyNumber->getNumber();
        $add_names = ["tomek", "ania", 'magda'];
        $show = true;
        return $this->render('hello/hi.html.twig', compact('name', 'add_names', 'show', 'number'));
    }

    /**
     * @Route(path="/redirect/{action}", requirements = {"action" = "hello|currentDate"})
     * @param string $action
     * @param RedirectResponse
     * @return \Exception
     */
    public function moveToAction(string $action): RedirectResponse
    {
        return $this->redirectToRoute($action, ['name' => 'some name']);
    }

    /**
     * @Route(path="/page/{author}/{page}", requirements={"page" = "\d+"})
     */
    public function page(string $author, int $page = 1)
    {
        return new Response("Welcome on page $page for $author");
    }
}
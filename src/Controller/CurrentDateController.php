<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CurrentDateController extends AbstractController
{
    /**
     * 
     * @Route(path="/index", name="currentDate", methods="POST")
     * @return Response
     * @throws
     */
    public function currentDate(): Response
    {
        $currentDate = new \DateTime();

        return $this->getDateResponse('Current date', $currentDate);
    }
    /**
     * 
     * @Route(path="/index",  methods="GET")
     * @return Response
     * @throws
     */
    public function tomorrowDate(): Response
    {
        $tomorrowDate = new \DateTime();
        $tomorrowDate->add(new \DateInterval('P1D'));

        return $this->getDateResponse('Tommorrow date', $tomorrowDate);
    }

    private function getDateResponse(string $title, \DateTime $dateTime)
    {
        $dateTime = $dateTime->format(DATE_ATOM);
        $html = <<< EOT
        <html>
        <body>
            <h1>$title</h1>
            <p>$dateTime</p>
        </body>
        </html>
        EOT;

        return new Response($html);
    }
}